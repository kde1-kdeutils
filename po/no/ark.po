# Norwegian translations for KDE Utils, ark
# Copyright (C) 1998 Free Software Foundation, Inc.
# Hans Petter Bieker <zerium@webindex.no>, 1998.
#
msgid ""
msgstr ""
"Project-Id-Version: KDE UTILS/ark\n"
"POT-Creation-Date: 1998-11-14 02:05+0100\n"
"PO-Revision-Date: 1998-03-02 20:48+0100\n"
"Last-Translator: Hans Petter Bieker <zerium@webindex.no>\n"
"Language-Team: no <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO_8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#: adddlg.cc:11
msgid "Add File Options"
msgstr "Legg til filvalg"

#: adddlg.cc:21
msgid "Store Full Path"
msgstr "Lagre komplett sti"

#: adddlg.cc:24
msgid "Only Add Newer Files"
msgstr "Bare legg til nye filer"

#: extractdlg.cc:14
msgid "Extract"
msgstr "Pakk ut"

#: extractdlg.cc:28
msgid "Destination"
msgstr "Bestemmelsessted"

#: extractdlg.cc:44
msgid "Extract Options"
msgstr "Dra ut valg"

#: extractdlg.cc:52
msgid "Preserve Permissions"
msgstr "Bevar tillatelser"

#: extractdlg.cc:57
msgid "Filenames to Lowercase"
msgstr "Filnavn med sm� bokstaver"

#: extractdlg.cc:68
msgid "All Files"
msgstr "Alle filer"

#: extractdlg.cc:73
msgid "Selected File"
msgstr "Valgte filer"

#: extractdlg.cc:78
msgid "Pattern"
msgstr "M�nster"

#: extractdlg.cc:190
msgid "Extract To"
msgstr "Pakk ut til"

#: arkwidget.cc:71
msgid "&New..."
msgstr "&Ny..."

#: arkwidget.cc:74
msgid "&Extract To..."
msgstr "Pakk &ut til..."

#: arkwidget.cc:79
msgid "E&xtract..."
msgstr "P&akk ut..."

#: arkwidget.cc:80
msgid "&View file"
msgstr "&Vis fil"

#: arkwidget.cc:82
msgid "&Delete file"
msgstr "&Slett fil"

#: arkwidget.cc:84
msgid "&Set Archive Directory..."
msgstr "&Sett arkivmappe..."

#: arkwidget.cc:85
msgid "Set &Tar Executable..."
msgstr "Velg &Tar-programfil..."

#: arkwidget.cc:86
msgid "&File Adding Options..."
msgstr "Valg for &filtillegging..."

#: arkwidget.cc:97
msgid "File Operations"
msgstr "Filoperasjoner"

#: arkwidget.cc:99
msgid "Extract..."
msgstr "Pakk ut..."

#: arkwidget.cc:100
msgid "View file"
msgstr "Vis fil"

#: arkwidget.cc:102
msgid "Delete file"
msgstr "Slett fil"

#: arkwidget.cc:114
msgid "Goto Archive Dir..."
msgstr "G� til arkivmappe..."

#: arkwidget.cc:117
msgid "Extract To.."
msgstr "Pakk ut til..."

#: arkwidget.cc:121
msgid "Exit"
msgstr "Avslutt"

#: arkwidget.cc:128
msgid "Welcome to ark..."
msgstr "Velkommen til ark..."

#: arkwidget.cc:235
msgid "Can't create archive of that type"
msgstr "Kan ikke lage arkiv av den typen"

#: arkwidget.cc:257
msgid "Create or open an archive first"
msgstr "Lag eller �pne et arkiv f�rst"

#: arkwidget.cc:300
msgid "Can't add directories with this archive type"
msgstr "Kan ikke legge til mapper med denne arkiv typen"

#: arkwidget.cc:302
msgid "Error saving to archive"
msgstr "Feil ved lagring til arkiv"

#: arkwidget.cc:312
msgid "Archive Dir"
msgstr "Arkivmappe"

#: arkwidget.cc:325
msgid "What runs GNU tar:"
msgstr "Hva kj�rer GNU tar:"

#: arkwidget.cc:376 arkwidget.cc:578
msgid "Unknown archive format"
msgstr "Ukjent arkivformat"

#: arkwidget.cc:396
msgid "Size"
msgstr "St�rrelse"

#: arkwidget.cc:402
msgid "Archive directory does not exist."
msgstr "Arkivmappe eksisterer ikke."

#: arkwidget.cc:418
msgid "Archive Directory"
msgstr "Arkivmappe"
